const sprintf = require('sprintf-js').sprintf;

function recCreateRoute(app, regexes, parent_url, node) {

  var server = app.server,
      dblegi = app.dblegi,
      debug = app.debug,
      logging = app.logging;

  var url = parent_url;

  // 1. Compute current route's url
  for (var index in node['args']) {
    var key = node['args'][index];
    url += regexes[key] ? sprintf( '/:%1$s(%2$s)', key, regexes[key] ) : sprintf( '/:%1$s', key );
  }

  // 2. Create Route
  server.get(url, (req, res) => {
    params = req.params;
    var debuglocal = debug;

    // Set per-request debug
    if( req.query && 'debug' in req.query) {
      debuglocal = true;
    }

    // Send a 307 HTTP redirect in prod or display the redirect in debug
    function redirect(url) {
      if( logging ) {
        console.log(req.url.replace(/^([^?]*)\?.*$/, '$1') + ' → ' + url);
      }
      if( debuglocal ) {
        res.type('txt').send(url);
      } else {
        res.redirect(307, url);
      }
    }

    // Send a 501 HTTP error because Légifrance does not implement some resource
    function error501legifrance( url ) {
      if( logging ) {
        console.log( req.url.replace(/^([^?]*)\?.*$/, '$1') + ' → 501 Not Implemented by Légifrance' );
      }
      res.status( 501 ).type( 'txt' ).send( url );
    }

    if(params.year && params.month && params.day) {
      params.date = sprintf('%s-%s-%s', params.year, (params.month.length == 2 ? '' : '0')+Number(params.month), (params.day.length == 2 ? '' : '0')+Number(params.day));
    }
    if( params.constitution ) {
      params.code = 'constitution';
    }
    var point_in_time = null, date_point_in_time = null;
    sql_text = 'SELECT id, cid, date_debut, date_fin FROM textes_versions WHERE %s LIMIT 2;';
    sql_text_where = '';
    sql_text_params = {};
    for(var key in params) {
      if(key == 'type' && params[key].toUpperCase() == 'LOI') {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + "nature IN ('LOI','LOI_CONSTIT','LOI_ORGANIQUE')";
      } else if(key == 'type') {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'nature = $' + key;
        sql_text_params['$'+key] = params[key].toUpperCase();
      } else if( key === 'code' ) {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + ( params[key] === 'code' ? "nature = 'CODE'" : "nature = 'CONSTITUTION'" );
      } else if( key === 'constitution' ) {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'titrefull = $titrefull';
        sql_text_params['$titrefull'] = 'Constitution du 4 octobre 1958';
      } else if(key == 'natural_identifier') {
        if(params[key].match(/^[A-Z]{4}[0-9]{7}[A-Z]$/)) {
          sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'nor = $' + key;
        } else if(params[key].match(/^[0-9]{2,4}-[0-9]+$/)) {
          sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'num = $' + key;
        }
        sql_text_params['$'+key] = params[key].toUpperCase();
      } else if( key === 'domain' ) {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'LOWER(titrefull) = $titrefull';
        var codename = params.domain.toLowerCase().replace(/_/g, ' ');
        if( codename === 'code de justice militaire' ) {
          codename = 'code de justice militaire (nouveau)';
        }
        sql_text_params['$titrefull'] = codename;
      } else if(key == 'date') {
        sql_text_where += ( sql_text_where ? ' AND ' : '' ) + 'date_texte = $' + key;
        sql_text_params['$'+key] = params[key];
      } else if( key == 'point_in_time') {
        point_in_time = params.point_in_time;
        date_point_in_time = point_in_time.substr( 0, 4 ) + '-' + point_in_time.substr( 4, 2 ) + '-' + point_in_time.substr( 6, 2 );
      }
    }
    dblegi.all(sprintf(sql_text, sql_text_where), sql_text_params, function(err_text, rows_text) {
      if(err_text) {
        res.sendStatus(500);
      } else if(rows_text.length == 0) {
        res.status(404).type('txt').send('Not Found\n\nThere are no responses: you should verify your parameters in order to remove contradictory parameters.');
      } else if(rows_text.length > 1) {
        res.status(300).type('txt').send('Multiple Choices\n\nThere are more than a single response: you should add more parameters in order to be properly redirected.');
      } else {
        var row_text = rows_text[0];
        if( date_point_in_time && date_point_in_time < row_text.date_debut ) {
          res.status(404).type('txt').send('Not Found\n\nYou requested a point in time before the initial date.');
        } else if( date_point_in_time && ( row_text.date_fin && row_text.date_fin !== '2999-01-01' ) && date_point_in_time >= row_text.date_fin ) {
          res.status(404).type('txt').send('Not Found\n\nYou requested a point in time after the abrogation date.');
        } else if(!params.level || params.level === 'texte') {
          if( !params.format || params.format === 'html' ) {
            if( params.version === 'jo' && params.type ) {
              redirect( 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=' + row_text.cid + '&categorieLien=id' );
            } else if( params.type ) {
              redirect( 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=' + row_text.cid + ( point_in_time ? '&dateTexte=' + point_in_time : '' ) );
            } else if( params.version === 'jo' && params.code ) {
              var date_debut = row_text.date_debut;
              date_debut = date_debut.substr( 0, 4 ) + date_debut.substr( 5, 2 ) + date_debut.substr( 8, 2 );
              redirect( 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=' + row_text.cid + '&dateTexte=' + date_debut );
            } else if( params.code ) {
              redirect( 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=' + row_text.id + ( point_in_time ? '&dateTexte=' + point_in_time : '' ) );
            } else  {
              res.sendStatus(500);
            }
          } else if( params.format === 'rtf' ) {
            if( params.version === 'jo' && params.type ) {
              redirect( 'https://www.legifrance.gouv.fr/telecharger_rtf.do?idTexte=' + row_text.cid );
            } else if( params.type ) {
              var today = new Date();
              today = '' + today.getFullYear() + (today.getMonth() < 9 ? '0' : '') + (today.getMonth()+1) + (today.getDate() < 10 ? '0' : '') + today.getDate();
              redirect( 'https://www.legifrance.gouv.fr/telecharger_rtf.do?idTexte=' + row_text.id + '&dateTexte=' + today );
            } else {
              erreur501legifrance( 'Not Implemented\n\nIt seems Légifrance has no RTF version for consolidated codes and constitutions.' );
            }
          } else if( params.format === 'rdf' ) {
            if( params.version === 'jo' && params.type ) {
              redirect( 'https://www.legifrance.gouv.fr/jo_rdf.do?cidTexte=' + row_text.cid );
            } else {
              error501legifrance( 'Not Implemented\n\nIt seems Légifrance has no RDF version for consolidated texts.' );
            }
          } else if( params.format === 'pdf' ) {
            if( params.version === 'jo' && params.type ) {
              error501legifrance( 'Not Directly Implemented\n\nIt seems Légifrance has no direct link to the PDF version for initial texts, we can give you this link which is a PDF encapsulated in the HTML:\n\nhttps://www.legifrance.gouv.fr/jo_pdf.do?id=' + row_text.cid );
            } else {
              error501legifrance( 'Not Implemented\n\nIt seems Légifrance has no PDF version for consolidated texts.' );
            }
          }
        } else if(params.level && params.level.match(/^article_[0-9A-Za-z.*_-]+$/) && params.version == 'jo') {
          if( logging ) {
            console.log( req.url.replace(/^([^?]*)\?.*$/, '$1') + ' → 501 Not Implemented by ELI-router' );
          }
          res.status( 501 ).type( 'txt' ).send( 'Not Implemented\n\nThe article identifiers are needed to be found in the JORF database, which is not implemented in ELI-router as of now.' );
        } else if(params.level && params.level.match(/^article_[0-9A-Za-z.*_-]+$/)) {
          article_num = params.level.substr(8);
          sql_article = "SELECT id, etat, date_debut, date_fin FROM articles WHERE cid = $cid AND num = $num;";
          sql_article_params = {'$cid': row_text.cid, '$num': article_num};
          dblegi.all(sql_article, sql_article_params, function(err_article, rows_article) {
            if(err_article) {
              res.sendStatus(500);
            } if(rows_article.length == 0) {
              res.status(404).type('txt').send('Not Found\n\nThere are no responses: you should verify your parameters in order to remove contradictory parameters.');
            } else {
              var idArticle = null;
              if( !point_in_time ) {
                for( var i in rows_article ) {
                  if( rows_article[i].etat === null || rows_article[i].etat === 'ABROGE_DIFF' || rows_article[i].etat === 'VIGUEUR' ) {
                    if( idArticle === null ) {
                      idArticle = rows_article[i].id;
                    } else {
                      idArticle = 0;
                    }
                  }
                }
              } else {
                for( var i in rows_article ) {
                  var date_debut = rows_article[i].date_debut === '2999-01-01' ? null : rows_article[i].date_debut,
                      date_fin = rows_article[i].date_fin === '2999-01-01' ? null : rows_article[i].date_fin;
                  if( ( date_debut === null || date_debut <= date_point_in_time ) && ( date_fin === null || date_point_in_time < date_fin ) ) {
                    if( idArticle === null ) {
                      idArticle = rows_article[i].id;
                    } else {
                      idArticle = 0;
                    }
                  }
                }
              }
              if( idArticle ) {
                if( !params.format || params.format === 'html' ) {
                  redirect( 'https://www.legifrance.gouv.fr/affichTexteArticle.do?cidTexte=' + row_text.cid + '&idArticle=' + idArticle + ( point_in_time ? '&dateTexte=' + point_in_time : '' ) );
                } else {
                  error501legifrance( 'Not Implemented\n\nIt seems Légifrance has no PDF or RTF or RDF version for articles in consolidated texts.' );
                }
              } else {
                res.status(300).type('txt').send('Multiple Choices\n\nThere are more than a single response: you should add more parameters in order to be properly redirected.');
              }
            }
          });
        } else {
          res.sendStatus(500);
        }
      }
    });
  });

  // 3. Recursive call to children routes
  for (var index in node['nodes']) {
    recCreateRoute(app, regexes, url, node['nodes'][index]);
  }
}

// These regexes restrict the values in each URL’s component
regexes = {
  'constitution': 'constitution',
  'code': 'code|constitution',
  'type': null,
  'year': '[0-9]{4}',
  'month': '[0-9]{1,2}',
  'day': '[0-9]{1,2}',
  'natural_identifier': '[A-Z]{4}[0-9]{7}[A-Z]|[0-9]{2,4}-[0-9]+',
  'domain': null,
  'version': 'jo|lc',
  'level': 'texte|article_[0-9A-Za-z.\\*_-]+',
  'point_in_time': '[0-9]{8}',
  'language': 'fr',
  'format': 'html|pdf|rtf|rdf',
};

// Define url tree based on the following list
// of known urls:
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{domain}/{version}/{level}/{point_in_time}/{language}/{format}
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}                           URI_ELI_FO
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}                 URI_ELI_F1_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}         URI_ELI_F2_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}/fr      URI_ELI_FR_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}/fr/html URI_ELI_HTML_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}/fr/pdf  URI_ELI_PDF_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}/fr/rtf  URI_ELI_RTF_OJ
// /eli/{type}/{year}/{month}/{day}/{natural_identifier}/{version}/{level}/fr/rdf  URI_ELI_RDF_OJ
// /eli/jo/{year}/{month}/{day}                                                    URI_ELI_JO
// /eli/jo/{year}/{month}/{day}/fr                                                 URI_ELI_JO_FR
// /eli/jo/{year}/{month}/{day}/fr/html                                            URI_ELI_JO_HTML
// /eli/jo/{year}/{month}/{day}/fr/pdf                                             URI_ELI_JO_PDF
root_nodes = [
  { 'args': ['constitution'],
    'nodes': [
      { 'args': ['version'],
        'nodes': [
          { 'args': ['level'],
            'nodes': [
              { 'args': ['point_in_time'],
                'nodes': [
                  { 'args': ['language'],
                    'nodes': [
                      { 'args': ['format'],
                        'nodes': []
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  { 'args': ['code', 'domain'],
    'nodes': [
      { 'args': ['version'],
        'nodes': [
          { 'args': ['level'],
            'nodes': [
              { 'args': ['point_in_time'],
                'nodes': [
                  { 'args': ['language'],
                    'nodes': [
                      { 'args': ['format'],
                        'nodes': []
                      }
                    ]
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  },
  { 'args': ['type', 'year', 'month', 'day', 'natural_identifier'],
    'nodes': [
      { 'args': ['version'],
        'nodes': [
          { 'args': ['level'],
            'nodes': [
              { 'args': ['language'],
                'nodes': [
                  { 'args': ['format'],
                    'nodes': []
                  }
                ]
              }
            ]
          }
        ]
      }
    ]
  }
];

function run(app) {
  for (var index in root_nodes) {
    recCreateRoute(app, regexes, '/legifrance/eli', root_nodes[index]);
  }
}

// Bind Routes
exports = module.exports = {
  "regexes": regexes,
  "root_nodes": root_nodes,
  "run": run
};

// vim: set ts=2 sw=2 sts=2 et:
