#!/usr/bin/env node

// Requirements
var fs = require('fs'),
    express = require('express'),
    sqlite3 = require('sqlite3'),
    route = require('./lib/routing.js'),
    l10n = require('./lib/l10n.js');

// Arguments
var dblegi = 'legi.sqlite',
    port = '8080',
    noindex = false,
    debug = false,
    logging = false;

for(var i in process.argv) {
  var arg = process.argv[i];
  if(i < 2) {
  } else if(arg.substr(0, 7) == '--port=') {
    port = arg.substr(7);
  } else if(arg == '--no-index') {
    noindex = true;
  } else if(arg == '--debug') {
    debug = true;
  } else if(arg == '--logging') {
    logging = true;
  } else if(arg.substr(0, 2) != '--') {
    dblegi = arg;
  }
}

// Errors in arguments
if(!dblegi || !fs.existsSync(dblegi)) {
  console.error('Missing legi.py SQLite database');
  process.exit(1);
}

// Initialize application
var app = {
  'server': express(),
  'dblegi': new sqlite3.Database( dblegi ),
  'debug': debug,
  'logging': logging,
};

route.run( app );

var examples = [
  ['/eli/loi/2018/12/28/2018-1317/jo/texte', 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037882341&categorieLien=id'],
  ['/eli/loi/2018/12/28/2018-1317/lc/texte', 'https://www.legifrance.gouv.fr/affichTexte.do?cidTexte=JORFTEXT000037882341'],
];

app.server.get('/', function (req, res) {

  // First get the language
  var strings = {}, lang = 'fr';
  if( req.query.lang in l10n ) {
    lang = req.query.lang;
  } else {
    var languages = req.get('Accept-Language').split(',');
    for( var language in languages ) {
      if( languages[language].replace(/^([^-;]*).*$/, '$1') in l10n ) {
        lang = languages[language].replace(/^([^-;]*).*$/, '$1');
        break;
      }
    }
  }
  strings = l10n[lang];

  // Prepare the list of URLs and regexes
  var urls = app.server._router.stack.filter(r => r.route).map(r => r.route.path);
  var listUrls = '';
  for(var i in urls) {
    if(urls[i] != '/' && urls[i] != '/robots.txt') {
      listUrls += '<li>' + urls[i].replace(/\([^\)]+\)/g, '') + '</li>';
    }
  }
  var listParams = '';
  for( var i in route.regexes ) {
    listParams += '<li>' + i + strings.colon + ' ' + ( route.regexes[i] === null ? '<i>' + strings.unconstrainedregex + '</i>' : route.regexes[i] ) + '</li>';
  }
  var host = 'secure' in req && 'host' in req.headers ? (req.secure ? 'https://' : 'http://') + req.headers.host : '';

  var translations = '';
  for( var language in l10n ) {
    translations += (translations ? ', ' : '') + '<a href="/?lang=' + language + '" style="text-decoration: underline' + ( language == lang ? '' : ' dotted' ) + ';">' + l10n[language].autonym + '</a>';
  }

  var ex = '';
  for( var i in examples ) {
    ex += '<li><a href="' + host + '/legifrance' + examples[i][0] + '">' + host + ' /legifrance ' + examples[i][0] + '</a> ' + strings.redirectsto + '<br />→ <a href="' + examples[i][1] + '">' + examples[i][1] + '</a></li>';
  }

  // Show main page
  res.send(
    '<!doctype html>\n' +
    '<html lang="' + lang + '">\n' +
      '<head>\n' +
        '  <title>' + strings.title + '</title>\n' +
        '  <style type="text/css">\n' +
        '    body { margin: 8px 10%; padding: 0 1em; border: 1px solid black; border-radius: 1em; }\n' +
        '    a { text-decoration: none; }\n' +
        '    h1 { text-align: center; margin-bottom: 0; }\n' +
        '    p.translations { text-align: center; font-size: smaller; }\n' +
        '    #examples li { line-height: 1.5; }\n' +
        '    #examples li + li { margin-top: 0.7em; }\n' +
        '  </style>\n' +
      '</head>\n' +
      '<body>' +
        '<h1>' + strings.title + '</h1>' + 
        '<p class="translations">~&nbsp;' + strings.translations + ' ' + translations + '&nbsp;~</p>' +
        '<p>' + strings.intro1 + '</p>' +
        '<p>' + strings.intro2 + '</p>' +
        '<p>' + strings.intro3 + '</p>' +
          '<ul id="examples">' +
            ex +
          '</ul>' +
        '<p><b>' + strings.intro4 + '</b></p>' +
        '<p>' + strings.intro5 + '</p>' +
        '<hr />' +
        //'<p>Possibly in the future it will display metadata about the requested text or redirect to another law repository.</p>' +
        '<p>' + strings.intro6 + '</p>' +
          '<ul>' + listUrls + '</ul>' +
        '<p>' + strings.intro7 + '</p>' +
          '<ul>' + listParams + '</ul>' +
        '<hr />' +
        '<p>' + strings.intro8 + '</p>' +
        '<ul>' +
          '<li><u>307 Temporary Redirect' + strings.colon + '</u> ' + strings.code307 + '</li>' +
          '<li><u>404 Not Found' + strings.colon + '</u> ' + strings.code404 + '</li>' +
          '<li><u>300 Multiple Choices' + strings.colon + '</u> ' + strings.code300 + '</li>' +
          '<li><u>500 Internal Server Error' + strings.colon + '</u> ' + strings.code500 + '</li>' +
          '<li><u>501 Not Implemented' + strings.colon + '</u> ' + strings.code501 + '</li>' +
          '<li><u>200 OK' + strings.colon + '</u> ' + strings.code200 + '</li>' +
        '</ul>' +
      '</body>\n' +
    '</html>'
  );
})

app.server.get('/robots.txt', function(req, res) {
  if(noindex) {
    res.type('txt').send('User-agent: *\nDisallow: /legifrance/eli\n');
  } else {
    res.sendStatus(404);
  }
});

app.server.use(function(req, res){
  res.status(400).type('txt').send('Bad Request\n\nYour supplied URL does not match any URL pattern listed on the homepage.');
});

app.server.listen(port);

function gracefulShutdown() {
  app.dblegi.close();
  process.exit();
}

// listen for TERM signal .e.g. kill 
process.on ('SIGTERM', gracefulShutdown);

// listen for INT signal e.g. Ctrl-C
process.on ('SIGINT', gracefulShutdown);   

// vim: set ts=2 sw=2 sts=2 et:
